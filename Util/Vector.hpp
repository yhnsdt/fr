#ifndef __VECTOR_H__
#define __VECTOR_H__

/* 実質
  Vector<1>
  Vector<2>
  Vector<3>
  に使うためだけのテンプレート
  例えば
  template < int DIM >
  class Node {
     Vector<DIM> m_pos;
  };
  のようなことがしたいが、次元によってVectorの挙動(CrossProductなど)を変えるには特殊化するしかない
  特殊化するときはメンバをすべて書かないと難しいので
  結局元のテンプレートでは最低限の宣言だけしかしてない
*/

#include <cmath>

#ifdef WINDOWS
#undef min
#undef max
static inline double round(const double& val) {    
    return floor(val + 0.5);
}
#endif

#include <cstdio>
#include <cfloat>
#include <algorithm>
#include <iostream>

template < size_t D, typename TYPE=double >
class Vector {

public:
    static constexpr size_t DIM = D;

protected:
    TYPE m_x[D];
};

template < typename TYPE >
class Vector<1,TYPE> {

public:
    static constexpr size_t DIM = 1;

public:
    Vector() {  }
    Vector( const double& x ) { Set( x ); }
    Vector( const Vector& v ) { Set( v ); }
    Vector( const double* v ) { Set( v ); }
    Vector( const float* v ) { Set( v ); }
    virtual ~Vector() { }

    virtual void Init() {
	m_x[0] = 0.0;
    }

    void Set( const double& x ) {
	this->m_x[0] = x;
    }

    virtual void Set( const double* v ) {
	m_x[0] = v[0];
    }

    virtual void Set( const float* v ) {
	m_x[0] = (TYPE)v[0];
    }

    /**
     * whether this-vector-self is practically zero vector
     */
    virtual bool IsZero( const double& precision=1e-10 ) const {
	return SquareLength() <= precision*precision;
    }

    /**
     * whether argument value v is practically zero
     */
    static bool IsZero( const double& v, const double& precision=1e-10 ) {
	return fabs( v ) <= precision;
    }
    
    virtual double DotProduct( const Vector& v ) const {
	return this->m_x[0]*v.m_x[0];
    }

    virtual long double DotProductL( const Vector& v ) const {
	return (long double)this->m_x[0]*(long double)v.m_x[0];
    }

    virtual double SquareLength() const {
	return this->DotProduct( *this );
    }
    
    virtual long double SquareLengthL() const {
	return this->DotProductL( *this );
    }

    virtual double Length() const {
	return (double)sqrtl( SquareLengthL() );
    }

    //normalize this self
    virtual void Normalize() {

	double leng = Length();

	//avoid division by zero
	if ( IsZero(leng, DBL_EPSILON) ) return;

	this->m_x[0] /= leng;
	//round off to zero if result is too small
	if ( IsZero(this->m_x[0], DBL_EPSILON) ) this->m_x[0] = 0.0;
    }

    //return normalized new instance
    virtual Vector Normalized() const {
	Vector res = *this;
	res.Normalize();
	return res;
    }

    virtual void Invert() {
	this->m_x[0] = 1.0/this->m_x[0];	
    }

    virtual void SquareComponents() {
	this->m_x[0] *= this->m_x[0];
    }


    /**
     * 足し算
     */
    virtual Vector operator + ( const Vector& other ) const {
	Vector res( *this );
	res += other;
	return res;
    }

    virtual const Vector& operator += ( const Vector& other ) {
	m_x[0] += other.m_x[0];	
	return *this;
    }

    /**
     * マイナス符号
     */
    virtual Vector operator - () const {
	Vector res;
	res.m_x[0] = -m_x[0];
	return res;
    }

    /**
     * 引き算
     */
    virtual Vector operator - ( const Vector& other ) const {
	Vector res( *this );
	res -= other;
	return res;
    }

    virtual const Vector& operator -= ( const Vector& other ) {
	m_x[0] -= other.m_x[0];
	return *this;
    }

    /**
     * 定数との掛け算 
     */
    template< typename PRIMITIVE_TYPE >
    Vector operator * ( const PRIMITIVE_TYPE& c ) const {
	Vector res( *this );
	res *= c;
	return res;
    }

    template< typename PRIMITIVE_TYPE >
    const Vector& operator *= ( const PRIMITIVE_TYPE& c ) {
	m_x[0] *= c;
	return *this;
    }
    
    /**
     * 定数との掛け算 
     */
    template< typename PRIMITIVE_TYPE >
    friend Vector operator * ( const PRIMITIVE_TYPE& c, const Vector& z ) {
	Vector res( z );
	res *= c;
	return res;
    }

    /**
     * 定数での割り算
     */
    template< typename PRIMITIVE_TYPE >
    Vector operator / ( const PRIMITIVE_TYPE& c ) const {
	Vector res( *this );
	res /= c;
	return res;
    }

    template< typename PRIMITIVE_TYPE >
    const Vector& operator /= ( const PRIMITIVE_TYPE& c ) {
	m_x[0] /= c;
	return *this;
    }
    

    /**
     * 成分毎(element-wise)の積
     */
    virtual Vector operator * ( const Vector& other ) const {
	Vector res( *this );
	res *= other;
	return res;
    }

    virtual const Vector& operator *= ( const Vector& other ) {
	m_x[0] *= other[0];
	return *this;
    }

    /**
     * コピー代入
     */
    virtual void Set( const Vector& other ) {
	m_x[0] = other.m_x[0];
    }

    virtual Vector& operator = ( const Vector& other ) {
	Set( other );
	return *this;
    }

    /**
     * 大小比較
     */
    virtual bool operator < ( const Vector& other ) const {
	if ( m_x[0] < other[0] ) return true;
	return false;
    }
    
    /**
     * 等価比較
     */
    virtual bool operator == ( const Vector& other ) const {

	double d = this->m_x[0] - other[0];

	//要素が0のときは絶対誤差, 0でないときは相対誤差
	if ( this->m_x[0] == 0.0 ) {
	    if ( !IsZero( d, 1e-10 ) ) return false;
	}
	else {
	    if ( !IsZero( d/this->m_x[0], 1e-10 ) ) return false;
	}

	return true;
    }

    /**
     * 最大要素
     */
    virtual const double& Max() const { return m_x[0]; }
    
    /**
     * 最小要素
     */
    virtual const double& Min() const { return m_x[0]; }

    /**
     * 絶対値付き最大要素
     * 比較に絶対値を使うが戻り値には絶対値はついていない
     */
    virtual const double& AbsMax() const { return m_x[0]; }
    /**
     * 絶対値付き最小要素
     * 比較に絶対値を使うが戻り値には絶対値はついていない
     */
    virtual const double& AbsMin() const { return m_x[0]; }
    
    /**
     * インデクス式アクセス
     */
    operator const double* () const { return m_x; }
    operator double* () { return m_x; }    

    /**
     * cout出力
     */
    friend std::ostream& operator << ( std::ostream& out, const Vector& t ) {
	//out.write( (const char*)t.m_x, sizeof(t.m_x) );
	out << t[0] << "\n";
	return out;
    }

    /**
     * cin入力
     */
    friend std::istream& operator >> ( std::istream& in, Vector& t ) {
	//	in.read( (char*)t.m_x, sizeof(t.m_x) );
	in >> t[0];
	return in;
    }

    virtual void Print( FILE *fpOut=stdout ) const {
	fprintf(fpOut, "%e\n", this->m_x[0]);
    }

    virtual void PrintEx( FILE *fpOut=stdout ) const {
	fprintf(fpOut, "%20.16e\n", this->m_x[0]);
    }

protected:
    TYPE m_x[1];

};


template < typename TYPE >
class Vector<2,TYPE> {

public:
    static constexpr size_t DIM = 2;

public:
    Vector() {  }
    Vector( const double& x, const double& y ) { Set( x, y ); }
    Vector( const Vector& v ) { Set( v ); }
    Vector( const double* v ) { Set( v ); }
    Vector( const float* v ) { Set( v ); }
    virtual ~Vector() { }

    virtual void Init() {
	m_x[0] = 0.0;
	m_x[1] = 0.0;
    }

    void Set( const double& x, const double& y ) {
	this->m_x[0] = x;
	this->m_x[1] = y;
    }
    
    virtual void Set( const double* v ) {
	m_x[0] = v[0];
	m_x[1] = v[1];
    }

    virtual void Set( const float* v ) {
	m_x[0] = (TYPE)v[0];
	m_x[1] = (TYPE)v[1];
    }

    /**
     * whether this-vector-self is practically zero vector
     */
    virtual bool IsZero( const double& precision=1e-10 ) const {
	return SquareLength() <= precision*precision;
    }

    /**
     * whether argument value v is practically zero
     */
    static bool IsZero( const double& v, const double& precision=1e-10 ) {
	return fabs( v ) <= precision;
    }
    
    virtual double DotProduct( const Vector& v ) const {
	return this->m_x[0]*v.m_x[0] + this->m_x[1]*v.m_x[1];
    }

    virtual long double DotProductL( const Vector& v ) const {
	return (long double)this->m_x[0]*(long double)v.m_x[0] +
	    (long double)this->m_x[1]*(long double)v.m_x[1];
    }

    virtual double SquareLength() const {
	return this->DotProduct( *this );
    }
    
    virtual long double SquareLengthL() const {
	return this->DotProductL( *this );
    }

    virtual double Length() const {
	return (double)sqrtl( SquareLengthL() );
    }

    //normalize this self
    virtual void Normalize() {

	double leng = Length();

	//avoid division by zero
	if ( IsZero(leng, DBL_EPSILON) ) return;

	this->m_x[0] /= leng;
	this->m_x[1] /= leng;

	//round off to zero if result is too small
	if ( IsZero(this->m_x[0], DBL_EPSILON) ) this->m_x[0] = 0.0;
	if ( IsZero(this->m_x[1], DBL_EPSILON) ) this->m_x[1] = 0.0;
    }

    //return normalized new instance
    virtual Vector Normalized() const {
	Vector res = *this;
	res.Normalize();
	return res;
    }

    virtual void Invert() {
	this->m_x[0] = 1.0/this->m_x[0];	
	this->m_x[1] = 1.0/this->m_x[1];
    }

    virtual void SquareComponents() {
	this->m_x[0] *= this->m_x[0];
	this->m_x[1] *= this->m_x[1];
    }

    double CrossProduct( const Vector& other ) const {
	return this->m_x[0]*other.m_x[1] - this->m_x[1]*other.m_x[0];
    }

    double TriangleArea( const Vector& a, const Vector& b ) const {
	Vector s = a;	s -= (*this);
	Vector t = b;	t -= (*this);
	return 0.5*s.CrossProduct( t );
    }

    /**
     * 足し算
     */
    virtual Vector operator + ( const Vector& other ) const {
	Vector res( *this );
	res += other;
	return res;
    }

    virtual const Vector& operator += ( const Vector& other ) {
	m_x[0] += other.m_x[0];	
	m_x[1] += other.m_x[1];	
	return *this;
    }

    /**
     * マイナス符号
     */
    virtual Vector operator - () const {
	Vector res;
	res.m_x[0] = -m_x[0];
	res.m_x[1] = -m_x[1];
	return res;
    }

    /**
     * 引き算
     */
    virtual Vector operator - ( const Vector& other ) const {
	Vector res( *this );
	res -= other;
	return res;
    }

    virtual const Vector& operator -= ( const Vector& other ) {
	m_x[0] -= other.m_x[0];
	m_x[1] -= other.m_x[1];
	return *this;
    }

    /**
     * 定数との掛け算 
     */
    template< typename PRIMITIVE_TYPE >
    Vector operator * ( const PRIMITIVE_TYPE& c ) const {
	Vector res( *this );
	res *= c;
	return res;
    }

    template< typename PRIMITIVE_TYPE >
    const Vector& operator *= ( const PRIMITIVE_TYPE& c ) {
	m_x[0] *= c;
	m_x[1] *= c;
	return *this;
    }
    
    /**
     * 定数との掛け算 
     */
    template< typename PRIMITIVE_TYPE >
    friend Vector operator * ( const PRIMITIVE_TYPE& c, const Vector& z ) {
	Vector res( z );
	res *= c;
	return res;
    }

    /**
     * 定数での割り算
     */
    template< typename PRIMITIVE_TYPE >
    Vector operator / ( const PRIMITIVE_TYPE& c ) const {
	Vector res( *this );
	res /= c;
	return res;
    }

    template< typename PRIMITIVE_TYPE >
    const Vector& operator /= ( const PRIMITIVE_TYPE& c ) {
	m_x[0] /= c;
	m_x[1] /= c;
	return *this;
    }
    

    /**
     * 成分毎(element-wise)の積
     */
    virtual Vector operator * ( const Vector& other ) const {
	Vector res( *this );
	res *= other;
	return res;
    }

    virtual const Vector& operator *= ( const Vector& other ) {
	m_x[0] *= other[0];
	m_x[1] *= other[1];
	return *this;
    }

    /**
     * コピー代入
     */
    virtual void Set( const Vector& other ) {
	m_x[0] = other.m_x[0];
	m_x[1] = other.m_x[1];
    }

    virtual Vector& operator = ( const Vector& other ) {
	Set( other );
	return *this;
    }

    /**
     * 大小比較
     */
    virtual bool operator < ( const Vector& other ) const {
	if ( m_x[0] < other[0] ) return true;
	if ( m_x[0] == other[0] && m_x[1] < other[1] ) return true;
	return false;
    }
    
    /**
     * 等価比較
     */
    virtual bool operator == ( const Vector& other ) const {

	//要素が0のときは絶対誤差, 0でないときは相対誤差
	double d = this->m_x[0] - other[0];
	if ( this->m_x[0] == 0.0 ) {
	    if ( !IsZero( d, 1e-10 ) ) return false;
	}
	else {
	    if ( !IsZero( d/this->m_x[0], 1e-10 ) ) return false;
	}

	d = this->m_x[1] - other[1];
	if ( this->m_x[1] == 0.0 ) {
	    if ( !IsZero( d, 1e-10 ) ) return false;
	}
	else {
	    if ( !IsZero( d/this->m_x[1], 1e-10 ) ) return false;
	}
	
	return true;
    }

    /**
     * 最大要素
     */
    virtual const TYPE& Max() const {
	return m_x[0] > m_x[1] ? m_x[0] : m_x[1];
    }
    
    /**
     * 最小要素
     */
    virtual const TYPE& Min() const {
	return m_x[0] < m_x[1] ? m_x[0] : m_x[1];
    }

    /**
     * 絶対値付き最大要素
     * 比較に絶対値を使うが戻り値には絶対値はついていない
     */
    virtual const TYPE& AbsMax() const {
	return std::abs(m_x[0]) > std::abs(m_x[1]) ? m_x[0] : m_x[1];
    }
    /**
     * 絶対値付き最小要素
     * 比較に絶対値を使うが戻り値には絶対値はついていない
     */
    virtual const TYPE& AbsMin() const {
	return std::abs(m_x[0]) < std::abs(m_x[1]) ? m_x[0] : m_x[1];
    }
    
    /**
     * インデクス式アクセス
     */
    operator const TYPE* () const { return m_x; }
    operator TYPE* () { return m_x; }    

    /**
     * cout出力
     */
    friend std::ostream& operator << ( std::ostream& out, const Vector& t ) {
	//out.write( (const char*)t.m_x, sizeof(t.m_x) );
	out << t[0] << " " << t[1] << "\n";
	return out;
    }

    /**
     * cin入力
     */
    friend std::istream& operator >> ( std::istream& in, Vector& t ) {
	//	in.read( (char*)t.m_x, sizeof(t.m_x) );
	in >> t[0] >> t[1];
	return in;
    }

    virtual void Print( FILE *fpOut=stdout ) const {
	fprintf(fpOut, "%e %e\n", this->m_x[0], this->m_x[1]);
    }

    virtual void PrintEx( FILE *fpOut=stdout ) const {
	fprintf(fpOut, "%20.16e %20.16e\n", this->m_x[0], this->m_x[1]);
    }

protected:
    TYPE m_x[2];

};

template < typename TYPE >
class Vector<3,TYPE> {

public:
    static constexpr size_t DIM = 3;

public:
    Vector() {  }
    Vector( const double& x, const double& y, const double& z ) { Set( x, y, z ); }
    Vector( const Vector& v ) { Set( v ); }
    Vector( const double* v ) { Set( v ); }
    Vector( const float* v ) { Set( v ); }
    virtual ~Vector() { }

    virtual void Init() {
	m_x[0] = 0.0;
	m_x[1] = 0.0;
	m_x[2] = 0.0;
    }

    void Set( const double& x, const double& y, const double& z ) {
	this->m_x[0] = x;
	this->m_x[1] = y;
	this->m_x[2] = z;
    }

    virtual void Set( const double* v ) {
	m_x[0] = v[0];
	m_x[1] = v[1];
	m_x[2] = v[2];
    }

    virtual void Set( const float* v ) {
	m_x[0] = (TYPE)v[0];
	m_x[1] = (TYPE)v[1];
	m_x[2] = (TYPE)v[2];
    }

    /**
     * whether this-vector-self is practically zero vector
     */
    virtual bool IsZero( const double& precision=1e-10 ) const {
	return SquareLength() <= precision*precision;
    }

    /**
     * whether argument value v is practically zero
     */
    static bool IsZero( const double& v, const double& precision=1e-10 ) {
	return fabs( v ) <= precision;
    }
    
    virtual double DotProduct( const Vector& v ) const {
	return this->m_x[0]*v.m_x[0] + this->m_x[1]*v.m_x[1] + this->m_x[2]*v.m_x[2];
    }

    virtual long double DotProductL( const Vector& v ) const {
	return (long double)this->m_x[0]*(long double)v.m_x[0] +
	    (long double)this->m_x[1]*(long double)v.m_x[1] +
	    (long double)this->m_x[2]*(long double)v.m_x[2];
    }

    virtual double SquareLength() const {
	return this->DotProduct( *this );
    }
    
    virtual long double SquareLengthL() const {
	return this->DotProductL( *this );
    }

    virtual double Length() const {
	return (double)sqrtl( SquareLengthL() );
    }

    //normalize this self
    virtual void Normalize() {

	double leng = Length();

	//avoid division by zero
	if ( IsZero(leng, DBL_EPSILON) ) return;

	this->m_x[0] /= leng;
	this->m_x[1] /= leng;
	this->m_x[2] /= leng;

	//round off to zero if result is too small
	if ( IsZero(this->m_x[0], DBL_EPSILON) ) this->m_x[0] = 0.0;
	if ( IsZero(this->m_x[1], DBL_EPSILON) ) this->m_x[1] = 0.0;
	if ( IsZero(this->m_x[2], DBL_EPSILON) ) this->m_x[2] = 0.0;
    }

    //return normalized new instance
    virtual Vector Normalized() const {
	Vector res = *this;
	res.Normalize();
	return res;
    }

    virtual void Invert() {
	this->m_x[0] = 1.0/this->m_x[0];	
	this->m_x[1] = 1.0/this->m_x[1];
	this->m_x[2] = 1.0/this->m_x[2];
    }

    virtual void SquareComponents() {
	this->m_x[0] *= this->m_x[0];
	this->m_x[1] *= this->m_x[1];
	this->m_x[2] *= this->m_x[2];
    }

    Vector CrossProduct( const Vector& other ) const {
	return Vector( this->m_x[1]*other.m_x[2] - this->m_x[2]*other.m_x[1],
		       this->m_x[2]*other.m_x[0] - this->m_x[0]*other.m_x[2],
		       this->m_x[0]*other.m_x[1] - this->m_x[1]*other.m_x[0] );
    }

    double TriangleArea( const Vector& a, const Vector& b ) const {
	Vector s = a; s -= *this;
	Vector t = b; t -= *this;
	return 0.5*s.CrossProduct( t ).Length();
    }
    
    Vector CalculateNormal( const Vector& p1, const Vector& p2 ) const {
	Vector v1 = p1; v1 -= *this;
	Vector v2 = p2; v2 -= *this;
	Vector norm = v1.CrossProduct( v2 );
	norm.Normalize();
	return norm;
    }
  
    static Vector CalculateNormal( const Vector& p0, 
				   const Vector& p1, const Vector& p2 ) {
	return p0.CalculateNormal(p1, p2);
    }
    
    void CreateOrthoSet( Vector& s, Vector& t ) const {
	
	s.Set( 1.0, 0.0, 0.0 );

	t = s.CrossProduct( *this );
	if ( t.IsZero() ) {
	    s.Set( 0.0, 1.0, 0.0 );
	    t = s.CrossProduct( *this );
	}

	s = this->CrossProduct( t );

	s.Normalize();
	t.Normalize();
    }

    void RotateAroundAxis( const Vector& rotAxis, const double& angle ) {

	Vector org( *this );

	double c = cos( angle );
	double s = sin( angle );

	Vector quartAxis1( rotAxis.CrossProduct(org) );
	quartAxis1 = quartAxis1.CrossProduct( rotAxis );
	quartAxis1.Normalize();

	Vector quartAxis2( org.CrossProduct(rotAxis) );
	quartAxis2.Normalize();

	Vector res = c*quartAxis1 + s*quartAxis2 + rotAxis.DotProduct(org)*rotAxis;
	this->m_x[0] = res[0];
	this->m_x[1] = res[1];
	this->m_x[2] = res[2];
    }

    void RotateAroundX( const double& angle ) {

	Vector org( *this );

	double c = cos( angle );
	double s = sin( angle );

	this->m_x[1] = c*org.m_x[1] + s*org.m_x[2];
	this->m_x[2] = c*org.m_x[2] - s*org.m_x[1];
    }

    void RotateAroundY( const double& angle ) {

	Vector org( *this );

	double c = cos( angle );
	double s = sin( angle );

	this->m_x[0] = c*org.m_x[0] - s*org.m_x[2];
	this->m_x[2] = s*org.m_x[0] + c*org.m_x[2];
    }

    /**
     * 足し算
     */
    virtual Vector operator + ( const Vector& other ) const {
	Vector res( *this );
	res += other;
	return res;
    }

    virtual const Vector& operator += ( const Vector& other ) {
	m_x[0] += other.m_x[0];	
	m_x[1] += other.m_x[1];	
	m_x[2] += other.m_x[2];	
	return *this;
    }

    /**
     * マイナス符号
     */
    virtual Vector operator - () const {
	Vector res;
	res.m_x[0] = -m_x[0];
	res.m_x[1] = -m_x[1];
	res.m_x[2] = -m_x[2];
	return res;
    }

    /**
     * 引き算
     */
    virtual Vector operator - ( const Vector& other ) const {
	Vector res( *this );
	res -= other;
	return res;
    }

    virtual const Vector& operator -= ( const Vector& other ) {
	m_x[0] -= other.m_x[0];
	m_x[1] -= other.m_x[1];
	m_x[2] -= other.m_x[2];
	return *this;
    }

    /**
     * 定数との掛け算 
     */
    template< typename PRIMITIVE_TYPE >
    Vector operator * ( const PRIMITIVE_TYPE& c ) const {
	Vector res( *this );
	res *= c;
	return res;
    }

    template< typename PRIMITIVE_TYPE >
    const Vector& operator *= ( const PRIMITIVE_TYPE& c ) {
	m_x[0] *= c;
	m_x[1] *= c;
	m_x[2] *= c;
	return *this;
    }
    
    /**
     * 定数との掛け算 
     */
    template< typename PRIMITIVE_TYPE >
    friend Vector operator * ( const PRIMITIVE_TYPE& c, const Vector& z ) {
	Vector res( z );
	res *= c;
	return res;
    }

    /**
     * 定数での割り算
     */
    template< typename PRIMITIVE_TYPE >
    Vector operator / ( const PRIMITIVE_TYPE& c ) const {
	Vector res( *this );
	res /= c;
	return res;
    }

    template< typename PRIMITIVE_TYPE >
    const Vector& operator /= ( const PRIMITIVE_TYPE& c ) {
	m_x[0] /= c;
	m_x[1] /= c;
	m_x[2] /= c;
	return *this;
    }
    

    /**
     * 成分毎(element-wise)の積
     */
    virtual Vector operator * ( const Vector& other ) const {
	Vector res( *this );
	res *= other;
	return res;
    }

    virtual const Vector& operator *= ( const Vector& other ) {
	m_x[0] *= other[0];
	m_x[1] *= other[1];
	m_x[2] *= other[2];
	return *this;
    }

    /**
     * コピー代入
     */
    virtual void Set( const Vector& other ) {
	m_x[0] = other.m_x[0];
	m_x[1] = other.m_x[1];
	m_x[2] = other.m_x[2];
    }

    virtual Vector& operator = ( const Vector& other ) {
	Set( other );
	return *this;
    }

    /**
     * 大小比較
     */
    virtual bool operator < ( const Vector& other ) const {
	if ( m_x[0] < other[0] ) return true;
	if ( m_x[0] == other[0] && m_x[1] < other[1] ) return true;
	if ( m_x[1] == other[1] && m_x[2] < other[2] ) return true;
	return false;
    }
    
    /**
     * 等価比較
     */
    virtual bool operator == ( const Vector& other ) const {

	//要素が0のときは絶対誤差, 0でないときは相対誤差
	for ( int i=0; i<3; ++i ) {
	    double d = this->m_x[i] - other[i];
	    if ( this->m_x[i] == 0.0 ) {
		if ( !IsZero( d, 1e-10 ) ) return false;
	    }
	    else {
		if ( !IsZero( d/this->m_x[i], 1e-10 ) ) return false;
	    }
	}
	
	return true;
    }

    /**
     * 最大要素
     */
    virtual const TYPE& Max() const { return *std::max_element( m_x, m_x+3 ); }
    
    /**
     * 最小要素
     */
    virtual const TYPE& Min() const { return *std::min_element( m_x, m_x+3 ); }

    /**
     * 絶対値付き最大要素
     * 比較に絶対値を使うが戻り値には絶対値はついていない
     */
    virtual const TYPE& AbsMax() const { return *std::max_element( m_x, m_x+3,
								   []( const TYPE& a, const TYPE& b ) {
								       return std::abs(a) < std::abs(b); } ); }
    /**
     * 絶対値付き最小要素
     * 比較に絶対値を使うが戻り値には絶対値はついていない
     */
    virtual const TYPE& AbsMin() const { return *std::min_element( m_x, m_x+3,
								   []( const TYPE& a, const TYPE& b ) {
								       return std::abs(a) < std::abs(b); } ); }
    
    /**
     * インデクス式アクセス
     */
    operator const TYPE* () const { return m_x; }
    operator TYPE* () { return m_x; }    
    
    /**
     * cout出力
     */
    friend std::ostream& operator << ( std::ostream& out, const Vector& t ) {
	//out.write( (const char*)t.m_x, sizeof(t.m_x) );
	out << t[0] << " " << t[1] << " " << t[2] << "\n";
	return out;
    }

    /**
     * cin入力
     */
    friend std::istream& operator >> ( std::istream& in, Vector& t ) {
	//	in.read( (char*)t.m_x, sizeof(t.m_x) );
	in >> t[0] >> t[1] >> t[2];
	return in;
    }

    virtual void Print( FILE *fpOut=stdout ) const {
	fprintf(fpOut, "%e %e %e\n", this->m_x[0], this->m_x[1], this->m_x[2]);
    }

    virtual void PrintEx( FILE *fpOut=stdout ) const {
	fprintf(fpOut, "%20.16e %20.16e %20.16e\n", this->m_x[0], this->m_x[1], this->m_x[2]);
    }

protected:
    TYPE m_x[3];

};


typedef Vector<1> Vector1d;
typedef Vector<2> Vector2d;
typedef Vector<3> Vector3d;

#endif //__VECTOR_H__
