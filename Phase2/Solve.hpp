#include <iostream>
#include <set>
#include <map>
#include "Vector.hpp"
#include <cassert>
#include <queue>
#include <vector>

using namespace std;
using Pii = pair<int,int>;

class Fragment {
public:
    vector<int> frame;
    vector< vector<int> > crack;
    vector< int > part_crack;

protected:
    void Shift() {

	int s = -1;
	for ( int i=0; i<frame.size()-1; ++i ) {

	    //crack tipは避ける
	    int l = i == 0 ? frame.size()-1 : i-1;
	    int r = i+1;
	    if ( frame[l] == frame[r] ) continue;
		
	    int j = 0;
	    for ( ; j<frame.size(); ++j ) {
		if ( i == j ) continue;
		if ( frame[i] == frame[j] ) break;
	    }

	    if ( j == frame.size() ) {
		//frame[i]は重複していない & crack tipではない ので輪郭上の点
		s = i;
		break;
	    }
	}
	assert( s >= 0 );

	//frameの開始点を輪郭上の点sとしてshift
	vector<int> tmp( frame );
	int j = 0;
	for ( int i=s; i<tmp.size(); ++i,++j ) frame[j] = tmp[i];
	for ( int i=0; i<s;          ++i,++j ) frame[j] = tmp[i];
    }
    
public:
    Fragment() {}
    void Append( int p ) { frame.push_back( p ); }

    void Fix() {

	Shift();

	queue<int> q;
	for ( auto v : frame ) q.push( v );

	frame.clear();
	frame.push_back( q.front() ); q.pop();
	frame.push_back( q.front() ); q.pop();

	while ( !q.empty() ) {

	    bool part = false;
	    while( frame[frame.size()-2] == q.front() ) {
		part_crack.push_back( frame.back() );
		frame.pop_back();
		q.pop();
		part = true;
	    }

	    if ( part ) {
		part_crack.push_back( frame.back() );
		crack.push_back( move(part_crack) );
	    }

	    if ( !q.empty() ) {
		frame.push_back( q.front() ); q.pop();
	    }
	}
	
    }
    friend ostream& operator << ( ostream& os, const Fragment& t ) {
	os << "frame: " << endl;
	for ( int v : t.frame ) os << " " << v << endl;
	os << "crack: " << endl;
	for ( int i=0; i<t.crack.size(); ++i ) {
	    os << " " << i << endl;
	    for ( int v : t.crack[i] ) os << "   " << v << endl;
	}
	return os;
    }
};

//IO utility
template< typename TYPE >
ostream& operator << ( ostream& os, const vector< vector<TYPE> >& vv ) {
    for ( int i=0; i<vv.size(); ++i ) {
	os << i << ": " << endl;
	for ( int v : vv[i] ) os << " " << v << endl;
    }
    return os;
}

ostream& operator << ( ostream& os, const Pii& p ) {
    os << p.first << " -- " << p.second;
    return os;
}

template< typename TYPE >
ostream& operator << ( ostream& os, const vector< TYPE >& t ) {
    for ( int i=0; i<t.size(); ++i ) {
	os << i << ": " << endl;
	os << t[i];
    }
    return os;
}

vector< Vector3d > point;
vector< vector<int> > edge;//ある点sから繋がる点tの番号

vector< set<int> > used;//ある点sから出る辺が繋がる先の点番号は連番とは限らない

vector< Fragment > frag;

void Input() {
    int n, m;
    cin >> n >> m;

    edge.resize( n );
    
    double x, y;
    for ( int i=0; i<n; ++i ) {
	cin >> x >> y;
	point.push_back( Vector3d(x, y, 0.0) );
    }

    int s, t;
    for ( int i=0; i<m; ++i ) {
	cin >> s >> t;
	edge[s].push_back( t );
	edge[t].push_back( s );
    }
}

//凹凸多角形共用　ただし、z座標が同じ平面上限定
//反時計回りなら符号が負になる
double GetArea( const vector<int>& p ) {
    double a = 0.0;
    for ( int i=0; i<p.size(); ++i ) {
	int p0 = p[i];
	int p1 = p[(i+1)%p.size()];
	const Vector3d& u = point[p0];
	const Vector3d& v = point[p1];
	a += u[0]*v[1] - u[1]*v[0];
    }
    return 0.5*a;
}

//まだ使われていない辺を探す
Pii FindUnUsed() {
    for ( int s=0; s<edge.size(); ++s ) {
	for ( int t : edge[s] ) {
	    if ( used[s].find( t ) == used[s].end() ) {
		return make_pair( s, t );
	    }
	}
    }
    return make_pair( -1, -1 );
}

//使った辺にマークをつける
void Use( const Pii& e ) {
    used[e.first].insert( e.second );
}

//現在居る辺curの次にたどるべき辺を返す
Pii FindNext( Pii cur ) {

    int prev = cur.first;
    int t = cur.second;
    const Vector3d& p0 = point[t];

    //前処理で全ての点回りにエッジの偏角を計算しておいてfindするのでも良い
    vector< pair<double,int> > n_list;
    for ( int d : edge[t] ) {
	Vector3d u = point[d] - p0;
	double angle = atan2( u[1], u[0] );
	n_list.push_back( make_pair(angle, d) );
    }
    
    sort( n_list.begin(), n_list.end() );

    if ( n_list[0].second == prev ) return make_pair( t, n_list.back().second );
    
    for ( int i=1; i<n_list.size(); ++i ) {
	if ( n_list[i].second == prev ) return make_pair( t, n_list[i-1].second );
    }

    assert( false );
}

void DFS( const Pii& cur, const Pii& init, Fragment& p ) {

    if ( cur == init ) return;

    Use( cur );
    p.Append( cur.first );

    DFS( FindNext(cur), init, p );
}

void DetectPoly() {

    used.resize( edge.size() );

    while ( true ) {

	//まだ使っていないedgeから始める
	Pii init_edge = FindUnUsed();
	if ( init_edge.first == -1 ) break;

	//新しいpolygonを作る
	Fragment p;

	Use( init_edge );
	p.Append( init_edge.first );

	DFS( FindNext(init_edge), init_edge, p );
	
	if ( GetArea(p.frame) > 0.0 ) frag.push_back( move(p) );
    }

    for ( auto& f : frag ) f.Fix();
    
    cout << frag << endl;
}

