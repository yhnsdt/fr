#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include "Solve.hpp"

int winW = 800, winH = 800;

double minX = 1e32, maxX = -1e32;
double minY = 1e32, maxY = -1e32;

void SetColor( int i ) {
    glColor3ub( 255*(i&1), 255*(i&2), 255*(i&4) );
}

void DrawPoints() {
    glPointSize( 10.0 );
    glColor3d( 0.0, 0.0, 0.0 );
    glBegin( GL_POINTS );
    for ( auto p : point ) {
	glVertex2dv( p );
    }
    glEnd();
}

void DrawEdges() {
    glColor3d( 0.0, 0.0, 1.0 );
    glBegin( GL_LINES );
    for ( int s=0; s<edge.size(); ++s ) {
	for ( int t : edge[s] ) {
	    glVertex2dv( point[s] );
	    glVertex2dv( point[t] );
	}
    }
    glEnd();
}

void DrawFragments() {
    for ( int i=0; i<frag.size(); ++i ) {

	SetColor( i+1 );

	glLineWidth( 2.0*(frag.size() - i) + 1.0 );
	glBegin( GL_LINE_LOOP );
	for ( int j : frag[i].frame ) glVertex3dv( point[j] );
	glEnd();

	glLineWidth( 1.0 );
	for ( auto v : frag[i].crack ) {
	    glBegin( GL_LINE_STRIP );
	    for ( int j : v ) glVertex3dv( point[j] );
	    glEnd();
	}
    }
}

void display(void) {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    glOrtho( minX, maxX, minY, maxY, -1.0, 1.0 );
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
    
    DrawPoints();
    DrawEdges();
    DrawFragments();

    glutSwapBuffers();
}

void reshape(int width, int height) {

    winW = width;
    winH = height;
    
    glViewport(0, 0, width, height);

    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    glOrtho( 0.0, (double)winW, 0.0, (double)winH, -1.0, 1.0 );

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
}

void keyboard(unsigned char key, int x, int y) {
    switch (key) {
    case '+':
	glutPostRedisplay();
	break;
    case '-':
	glutPostRedisplay();
	break;
    case 27:
	exit(0);
	break;
    default:
	break;
    }
}

void init(void) {

    glClearColor(0.5, 0.5, 0.5, 1.0);

    const vector< Vector3d >& pos = point;
    for ( const Vector3d& p : pos ) {
	minX = min( minX, p[0] );
	maxX = max( maxX, p[0] );
	minY = min( minY, p[1] );
	maxY = max( maxY, p[1] );
    }
    
    double max_leng = max( maxX - minX, maxY - minY );
    maxX = minX + max_leng;
    maxY = minY + max_leng;
    double dx = maxX - minX;
    double dy = maxY - minY;
    minX -= 0.05*dx;
    maxX += 0.05*dx;
    minY -= 0.05*dy;
    maxY += 0.05*dy;
}

int main(int argc, char *argv[]) {

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize( winW, winH );
    glutCreateWindow("visualizer");

    Input();
    DetectPoly();
    
    init();

    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutReshapeFunc(reshape);

    glutMainLoop();
    return 0;
}

