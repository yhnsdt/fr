#include <iostream>
#include <set>
#include <map>
#include <vector>
#include "Vector.hpp"
#include <cassert>

using namespace std;
using Pii = pair<int,int>;

//IO utilities
template< typename TYPE >
ostream& operator << ( ostream& os, const vector< vector<TYPE> >& vv ) {
    for ( int i=0; i<vv.size(); ++i ) {
	os << i << ": " << endl;
	for ( int v : vv[i] ) os << " " << v << endl;
    }
    return os;
}

ostream& operator << ( ostream& os, const Pii& p ) {
    os << p.first << " -- " << p.second;
    return os;
}


vector< Vector3d > point;
vector< vector<int> > edge;//ある点sから繋がる点tの番号
vector< vector<int> > poly;
vector< set<int> > used;//ある点sから出る辺が繋がる先の点番号は連番とは限らない

void Input() {
    int n, m;
    cin >> n >> m;

    edge.resize( n );
    
    double x, y;
    for ( int i=0; i<n; ++i ) {
	cin >> x >> y;
	point.push_back( Vector3d(x, y, 0.0) );
    }

    int s, t;
    for ( int i=0; i<m; ++i ) {
	cin >> s >> t;
	edge[s].push_back( t );
	edge[t].push_back( s );
    }
}

//凹凸多角形共用　ただし、z座標が同じ平面上限定
//時計回りなら符号が負になる
double GetArea( const vector<int>& p ) {
    double a = 0.0;
    for ( int i=0; i<p.size(); ++i ) {
	int p0 = p[i];
	int p1 = p[(i+1)%p.size()];
	const Vector3d& u = point[p0];
	const Vector3d& v = point[p1];
	a += u[0]*v[1] - u[1]*v[0];
    }
    return 0.5*a;
}

//まだ使ってない辺を探す
Pii FindUnUsed() {
    for ( int s=0; s<edge.size(); ++s ) {
	for ( int t : edge[s] ) {
	    if ( used[s].find( t ) == used[s].end() ) {
		return make_pair( s, t );
	    }
	}
    }
    return make_pair( -1, -1 );
}

//辺使用済みチェック
void Use( const Pii& e ) {
    used[e.first].insert( e.second );
}

//辺curの次に繋がる辺を偏角ソートして探す
Pii FindNext( Pii cur ) {

    int prev = cur.first;
    int t = cur.second;
    const Vector3d& p0 = point[t];

    vector< pair<double,int> > n_list;
    for ( auto d : edge[t] ) {
	Vector3d u = point[d] - p0;
	double angle = atan2( u[1], u[0] );
	n_list.push_back( make_pair(angle, d) );
    }
    sort( n_list.begin(), n_list.end() );

    if ( n_list[0].second == prev ) return make_pair( t, n_list.back().second );
    for ( int i=1; i<n_list.size(); ++i ) {
	if ( n_list[i].second == prev ) return make_pair( t, n_list[i-1].second );
    }

    assert( false );
}

void DFS( const Pii& cur, const Pii& init, vector<int>& p ) {

    if ( cur == init ) return;

    Use( cur );
    p.push_back( cur.first );

    DFS( FindNext(cur), init, p );
}

void DetectPoly() {

    used.resize( edge.size() );

    while ( true ) {

	Pii init_edge = FindUnUsed();
	if ( init_edge.first == -1 ) break;

	vector<int> p;
	Use( init_edge );
	p.push_back( init_edge.first );

	DFS( FindNext(init_edge), init_edge, p );

	if ( GetArea(p) > 0.0 ) poly.push_back( move(p) );
    }

    cout << poly << endl;
}

