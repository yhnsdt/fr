
int n, m;
vector< Vector3d > point;
vector< vector<int> > edge;
vector< vector<int> > poly;

map< pair<int,int>, bool > used;
vector< set<int> > used;


input() {
    //    read n, m, point, edge;
    cin >> n >> m;

    double x, y;
    for ( int i=0; i<n; ++i ) {
	cin >> x >> y;
	point.push_back( Vector3d( x, y, 0 ) );
    }

    edge.resize( n );
    
    int s, t;
    for ( int i=0; i<m; ++i ) {
	cin >> s >> t;
	edge[s].push_back( t );
	edge[t].push_back( s );
    }
}

pair<int,int> find_next( Edge curr ) {
    int prev = curr.p0;
    int t = curr.p1;

    //t回りにedgeを偏角でソート atan2(y,x)が使える

    //currから反時計回りに見た時に隣のedgeを返す
    
}

DFS( current_edge, initial_edge, p ) {

    if ( initial_edge == current_edge ) {
	return;
    }

    p.push_back( current_edge.p0 );
    used.insert( current_edge );
    
    DFS( find_next( current_edge ), initial_edge, p );
}

pair<int,int> find_unused_edge() {

    //...
    for ( int i=0; i<m; ++i ) {
	for ( int dst : edge[i] ) {
	    if ( used.find( i, dst ) == used.end() ) return make_pair( i, dst );
	}
    }
    
    return make_pair( -1, -1 );
}

detect_poly() {

    while ( true ) {

	vector<int> new_poly;

	pair<int,int> initial_edge = find_unused_edge();
	if ( initial_edge.first == -1 ) break;

	used.insert( initial_edge );

	new_poly.push_back( initial_edge.p0 );

	DFS( find_next(initial_edge), initial_edge, new_poly );

	poly.push_back( new_poly );
    }
    
}

int main() {
    input();
    detect_poly();
}
